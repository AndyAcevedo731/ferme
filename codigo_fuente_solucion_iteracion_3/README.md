                                           __
                               _____....--' .'
                     ___...---'._ o      -`(
           ___...---'            \   .--.  `\
 ___...---'                      |   \   \ `|
|                                |o o |  |  |
|                                 \___'.-`.  '.
|                                      |   `---'
'^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^' 


# FERME, Proyecto de título: Andrés Acevedo, Claudia Gómez, Gustavo Holley

Seguir paso a paso para la correcta instalación de este proyecto.

# Instalación:

## 1. Es necesario instalar Python 3.9
La versión 32 o 64 bits dependerá de los componentes del equipo utilizado.
Se puede descargar desde el siguiente link:
https://www.python.org/downloads/release/python-395/

## 2. Instalar base de datos

Para el proyecto Ferme, se decidió ocupar Oracle 18c Express edition.
Se puede descargar desde el siguiente link:

https://www.oracle.com/cl/database/technologies/xe-downloads.html

Es importante guardar el usuario y contraseña system creado durante la instalación, ya que será ocupado más adelante.

## 3. Descomprimir archivos

Descomprimir carpeta  ‘codigo_fuente_solucion_iteracion_3.rar’ y “venv” en disco local (C:).

## 4. Preparación del ambiente virtual

Para ocupar el ambiente virtual, existen dos alternativas: 
Crear un nuevo ambiente virtual
Se puede crear un nuevo entorno virtual abriendo una consola cmd en la carpeta que desee ocupar con el siguiente comando:

    python -m venv entornovirtual

El nombre “entornovirtual” es sugerido, se le puede cambiar al que más le acomode.
Luego nos dirigimos a la carpeta Scripts del ambiente, en ella abrimos el terminal cmd y activamos el ambiente virtual mediante el comando activate.

Navegamos a la carpeta de proyecto (ferme) e instalamos las dependencias.

    pip install -r requirements.txt

Si las desea instalar de forma manual:

arabic-reshaper==2.1.3
asgiref==3.2.10
astroid==2.5.3
colorama==0.4.4
cx-Oracle==8.2.1
Django==3.1
future==0.18.2
html5lib==1.1
isort==5.8.0
lazy-object-proxy==1.6.0
mccabe==0.6.1
Pillow==8.3.1
pylint==2.7.4
PyPDF2==1.26.0
python-bidi==0.4.2
pytz==2021.1
reportlab==3.5.68
six==1.16.0
sqlparse==0.4.1
toml==0.10.2
webencodings==0.5.1
wrapt==1.12.1
xhtml2pdf==0.2.5

## 3. Instalación de BD y Creación de usuario

3.1 Ingresar a la conexión system
Ingresar con el usuario ‘system’ y la contraseña que haya creado al momento de instalar Oracle.

3.2 Creación de usuario
Creamos el usuario PORTAFOLIO (si arroja error por caracteres ASCII, escribir C##PORTAFOLIO) el cual será el encargado de generar las tablas de nuestra aplicación django.
Luego en una hoja de trabajo crear el usuario portafolio con las siguientes sentencias:

alter session set "_oracle_script"=true;

DROP USER PORTAFOLIO CASCADE;
CREATE USER PORTAFOLIO IDENTIFIED BY PORTAFOLIO
DEFAULT TABLESPACE "USERS"
TEMPORARY TABLESPACE "TEMP";

-- QUOTAS
ALTER USER PORTAFOLIO QUOTA UNLIMITED ON USERS;

-- ROLES
GRANT "CONNECT" TO PORTAFOLIO;
GRANT "RESOURCE" TO PORTAFOLIO;
ALTER USER PORTAFOLIO DEFAULT ROLE "CONNECT","RESOURCE";

## 4. Crear conexión

Creamos la conexión para el usuario portafolio
Nombre: PORTAFOLIO
Usuario: PORTAFOLIO
Contraseña: PORTAFOLIO

## 5. Migración de datos

Con el ambiente virtual activado y estando en la carpeta de ferme 

--ejemplo (C:\myvenv\ferme\ferme)-- , ejecutamos el comando para crear las tablas en la base de datos:


python manage.py makemigrations

Una vez migrado, usar el siguiente comando: 

python manage.py migrate

## 6. Poblamiento de la base de datos
 Ejecutamos las sentencias que se encuentran en el archivo ‘carga_datos.txt’.
(Es necesario ejecutar estas sentencias incluso si no se van a cargar los datos de prueba para el correcto funcionamiento del sistema).

## 7. Crear superusuario

Para ingresar al sistema, es necesario crear un superusuario. Sin embargo puede ocupar las credenciales que ya vienen incluidas en el proyecto en el archivo "credenciales_usuarios.txt" (estas serán válidas sólo si realiza la carga de datos mencionadas en el punto 6).
En su defecto, podrá crear un superusuario mediante el siguiente comando:

python manage.py createsuperuser

## 8. Activar servidor

Con el ambiente virtual activado y estando en la carpeta de ferme, ejecutamos:

python manage.py runserver 

## 9. Ir a la ruta de la página 

Nos dirigimos a la dirección http://127.0.0.1:8000/ o en su defecto: http://localhost:8000 en el navegador de su preferencia (aunque recomendamos firefox para una mejor experiencia de uso), y con esto nuestra aplicación está 100% lista para ser utilizada.







